#! /usr/bin/env python3

import random

STRAIGHT_FLUSH = 8
FOUR_OF_KIND = 7
FULL_HOUSE = 6
FLUSH = 5
STRAIGHT = 4
THREE_OF_KIND = 3
TWO_PAIR = 2
ONE_PAIR = 1
HIGH_CARD = 0

def printError(msg):
    print(f'\033[91m{msg}\033[0m')

class ParseError(Exception):
    pass

class Card:
    def __init__(self, rankStr, suitStr):
        if rankStr in 'Jj':
            self.rank_val = 11
            self.rank_str = 'Jack'
        elif rankStr in 'Qq':
            self.rank_val = 12
            self.rank_str = 'Queen'
        elif rankStr in 'Kk':
            self.rank_val = 13
            self.rank_str = 'King'
        elif rankStr in 'Aa':
            self.rank_val = 14
            self.rank_str = 'Ace'
        else:
            try:
                self.rank_val = int(rankStr)
                self.rank_str = rankStr
                if self.rank_val < 2 or self.rank_val > 10:
                    raise ValueError
            except ValueError:
                raise ParseError("Invalid rank")
        if suitStr in 'Hh':
            self.suit = 'Hearts'
        elif suitStr in 'Dd':
            self.suit = 'Diamonds'
        elif suitStr in 'Cc':
            self.suit = 'Clubs'
        elif suitStr in 'Ss':
            self.suit = 'Spades'
        else:
            raise ParseError(f"Invalid suit: {suitStr}")

    def __eq__(self, other_card):
        return self.rank_val == other_card.rank_val and \
                self.suit == other_card.suit

    def __lt__(self, other_card):
        return self.rank_val < other_card.rank_val

    def __str__(self):
        return f'{self.rank_str} {self.suit}'

def genDeck():
    deck = []
    rankList = [str(val) for val in range(2, 11)]
    rankList = rankList + ['J', 'Q', 'K', 'A']
    for rank_str in rankList:
        for suit_str in ['H', 'D', 'C', 'S']:
            deck.append(Card(rank_str, suit_str))
    return deck

def parseCard(input_str):
    card = None
    if len(input_str) == 2:
        card = Card(input_str[0], input_str[1])
    elif len(input_str) == 3:
        card = Card(input_str[:1], input_str[2])
    else:
        raise ParseError("Invalid rank/suit")
    return card

def printCards(cards):
    for c in cards:
        print(c)

def drawRandCard(deck):
    index = random.randrange(0, len(deck))
    return deck.pop(index)

def drawHand(deck):
    return [drawRandCard(deck), drawRandCard(deck)]

def checkStraights(cardList):
    # TODO handle aces
    bestStraightIndex = -1
    straightCount = 0
    sameSuit = True
    prevCard = None
    for i, c in enumerate(cardList):
        if prevCard is None or prevCard.rank_val - c.rank_val == 1:
            prevCard = c
            straightCount = straightCount + 1

            if straightCount == 5:
                bestStraightIndex = i - 4
                if sameSuit:
                    break
                # reset
                straightCount = 1
                sameSuit = True

            if prevCard.suit != c.suit:
                sameSuit = False
        else:
            # reset
            straightCount = 1
            sameSuit = True

    # Determine result
    if bestStraightIndex != -1:
        handVal = cardList[bestStraightIndex].rank_val
        handType = STRAIGHT
        if sameSuit:
            handType = STRAIGHT_FLUSH
        return handType, handVal
    return None, None

def best_hand(cardList):
    cardList.sort(reverse=True)
    handType, handVal = checkStraights(cardList)
    if handType:
        printCards(cardList)
        print("==")
    # straight flush (includes royal)
    # 4 of a kind
    # full house
    # flush
    # straight
    # 3 of a kind
    # 2 pair
    # pair
    # return type of hand as value, and the sorted hand for comparing draws

def simulate(num_players, hand, deck, comm_cards):
    deck = list(deck)
    oppHandList = []
    for i in range(num_players):
        oppHandList.append(drawHand(deck))
    while (len(comm_cards) < 5):
        comm_cards.append(drawRandCard(deck))
    best_hand(hand + comm_cards)

deck = genDeck()
print(len(deck))

while (True):
    line = input('Enter number players: ')
    try:
        num_players = int(line)
    except ValueError:
        printError('Invalid number of players')
        continue
    break

hand = []
while (len(hand) < 2):
    print(f'Enter your hand {len(hand) + 1}/2: ')
    line = input()
    try:
        card = parseCard(line)
    except ParseError as e:
        printError(e)
        continue
    if card not in deck:
        printError('Card already drawn')
        continue
    deck.remove(card)
    hand.append(card)

for i in range(1, 1000):
    comm_cards = []
    simulate(num_players, hand, deck, comm_cards)
